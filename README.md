# Backend RestAPI using nodejs express 
FancyCalculator service;
<br>
This app was created by Yoga Bhiantara 2018<br>
Instalation:<br>
1. Clone Backend RestAPI Fancy-calculator
2. Create database with name "db_calculator"
3. Import file sql "db_calculator.sql"
4. Open project and "npm install" module
5. Setup your database config "backend-restapi-fancy-calculator\config\database.js"
6. Start service with command "node index"
7. Node app is running on port 8080

Getting something wrong? 
you can contact me: gusss.yoga@gmail.com