var db = require('../config/database').db;

function addhistory(req, res) {
    const hitung = req.body.hitung;
    const hasil = req.body.hasil;
    const spesial = req.body.spesial ? req.body.spesial : '';
    if(!hitung || !hasil){
        return res.send({ error: false, message: 'Data masih ada yang kosong!' });
    }else {
        db.query("INSERT INTO history SET ? ", {
            hitung: hitung,
            hasil: hasil,
            spesial: spesial
        }, function (error) {
            if (error) throw error;
            return res.send({error: false, message: 'Data berhasil ditambah'});
        });
    }
}
module.exports = addhistory;