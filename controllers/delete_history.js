var db = require('../config/database').db;

function deletehistory(req, res) {
    const id_history = req.body.id_history;
    db.query("DELETE FROM history WHERE id_history = ? ", id_history, function (error) {
        if (error) throw error;
        return res.send({error: false, message: 'Data berhasil di hapus'});
    });
}

module.exports = deletehistory;