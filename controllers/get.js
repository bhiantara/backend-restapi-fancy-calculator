var db = require('../config/database').db;
var express = require('express');
var routes = express.Router();

function getdata(req, res) {
    db.query('SELECT * FROM history ORDER BY id_history DESC LIMIT 10', function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'Data berhasil diambil' });
    });
}

module.exports = getdata;