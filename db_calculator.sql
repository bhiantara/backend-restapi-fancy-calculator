/*
Navicat MySQL Data Transfer

Source Server         : DB_Local
Source Server Version : 100133
Source Host           : localhost:3306
Source Database       : db_calculator

Target Server Type    : MYSQL
Target Server Version : 100133
File Encoding         : 65001

Date: 2018-11-22 20:50:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for history
-- ----------------------------
DROP TABLE IF EXISTS `history`;
CREATE TABLE `history` (
  `id_history` int(11) NOT NULL AUTO_INCREMENT,
  `hitung` varchar(30) DEFAULT NULL,
  `hasil` varchar(30) DEFAULT NULL,
  `spesial` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id_history`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of history
-- ----------------------------
INSERT INTO `history` VALUES ('1', '7*7', '49', '');
INSERT INTO `history` VALUES ('2', '4+9*7', '67', '');
INSERT INTO `history` VALUES ('3', '9/3', '3', '');
INSERT INTO `history` VALUES ('4', '56/7', '8', '');
INSERT INTO `history` VALUES ('6', '99/4', '24.75', '');
