function control(req, res, next) {
    res.header('Access-Control-Allow-Origin', 'http://localhost:4200');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS,OPTION');
    res.header('Accept-Encoding', 'gzip');
    res.header("Access-Control-Allow-Headers", "token, Content-Type, Origin, X-Requested-With,  Accept");

    if (req.method === 'OPTIONS') {
        res.sendStatus(200);
    } else {
        next();
    }
}

module.exports = control;