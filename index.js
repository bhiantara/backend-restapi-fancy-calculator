const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const db = require('./config/database').db;
const control = require('./config/control');
const history = require('./controllers/get.js');
const historybyid = require('./controllers/getbyid');
const add_history = require('./controllers/add_history');
const delete_history = require('./controllers/delete_history');
var router = express.Router();
//start API
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

// default route allow origin website
app.use(control);

// connect to database
db.connect(function(err){
    if(err === null) {
        console.log("Database is connected");
    } else {
        console.log("Error connecting database");
    }
});

// get data
app.use('/history', history);
app.use('/historybyid', historybyid);

// post data
app.post('/add_history', add_history);
app.post('/delete_history', delete_history);

// port must be set to 8080 because incoming http requests are routed from port 80 to port 8080
app.listen(8080, function () {
    console.log('Node app is running on port 8080');
});

module.exports = app;